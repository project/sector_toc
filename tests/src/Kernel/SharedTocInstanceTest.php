<?php

namespace Drupal\Tests\sector_toc\Kernel;

use Drupal\Core\Render\RenderContext;
use Drupal\filter\Entity\FilterFormat;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\sector_toc\Plugin\Block\SectorTocBlock;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the ToC is only generated once.
 *
 * @group sector_toc
 */
class SharedTocInstanceTest extends EntityKernelTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'sector_toc',
    'chunker',
    'toc_api',
    'text',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['node', 'sector_toc']);
    $this->createContentType(['type' => 'resource']);
    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'filters' => [],
    ])->save();
  }

  /**
   * Test the formatter.
   */
  public function testSingleTocInstance() {
    $node = $this->createNode([
      'body' => [
        'value' => '<h2>Foo</h2><p>Bar</p><h2>Zip</h2><p>Zap</p><h2>ABC</h2><p>XYZ</p>',
        'format' => 'full_html',
      ],
      'type' => 'resource',
    ]);

    $currentRoute = $this->getMockBuilder('\Drupal\Core\Routing\RouteMatch')
      ->disableOriginalConstructor()
      ->getMock();
    $currentRoute->expects($this->any())
      ->method('getRouteName')
      ->willReturn('entity.node.canonical');
    $currentRoute->expects($this->any())
      ->method('getParameter')
      ->willReturn($node);

    $block = new SectorTocBlock(
      ['provider' => 'sector_toc'],
      'sector_toc_block',
      ['provider' => 'sector_toc'],
      $currentRoute,
    );

    $renderer = \Drupal::service('renderer');
    $context = new RenderContext();

    $renderer->executeInRenderContext($context, function () use ($block, $node, $renderer) {
      $node->get('body')->view([
        'type' => 'toc_chunker',
        'settings' => ['toc_type' => 'sector_toc'],
      ]);
      $block->build();
    });

    // Only 1 ToC should be present as the ID is shared.
    $tocManager = \Drupal::service('toc_api.manager');
    $tocInstancesProperty = new \ReflectionProperty('\Drupal\toc_api\TocManager', 'tocs');
    $tocInstances = $tocInstancesProperty->getValue($tocManager);
    $this->assertCount(1, $tocInstances);
  }

}
