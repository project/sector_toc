<?php

namespace Drupal\Tests\sector_toc\Kernel;

use Drupal\Core\Render\RenderContext;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the formatter functionality.
 *
 * @group sector_toc
 */
class TocChunkerFormatterTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sector_toc', 'chunker', 'toc_api', 'text'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['sector_toc']);

    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'filters' => [],
    ])->save();

    FieldStorageConfig::create([
      'field_name' => 'formatted_text',
      'entity_type' => 'entity_test',
      'type' => 'text_long',
      'settings' => [],
    ])->save();

    FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'formatted_text',
      'label' => 'Filtered text',
    ])->save();
  }

  /**
   * Test the formatter.
   */
  public function testFormatter() {
    $content = <<<HTML
<h2>H2 Section 1</h2>
<p>Content section 1</p>
<h2>H2 Section 2</h2>
<p>Content section 2</p>
<h2>H2 Section 3</h2>
<p>Content section 3</p>
<h2>H2 Section 4</h2>
<p>Content section 4</p>
<h2>H2 Section 5</h2>
<p>Content section 4</p>
HTML;

    // Create the entity.
    $entity = $this->container->get('entity_type.manager')
      ->getStorage('entity_test')
      ->create(['name' => $this->randomMachineName()]);
    $entity->formatted_text = [
      'value' => $content,
      'format' => 'full_html',
    ];
    $entity->save();

    $renderer = \Drupal::service('renderer');
    $context = new RenderContext();

    $output = $renderer->executeInRenderContext($context, function () use ($renderer, $entity) {
      // Test default settings.
      $field_build = $entity->get('formatted_text')->view([
        'type' => 'toc_chunker',
        'settings' => ['toc_type' => 'sector_toc'],
      ]);
      return $renderer->render($field_build[0]);
    });

    $expected = <<<HTML
<div class="chunker-section"><h2 id="h2-section-1">H2 Section 1</h2>
<p>Content section 1</p>
</div><div class="chunker-section"><h2 id="h2-section-2">H2 Section 2</h2>
<p>Content section 2</p>
</div><div class="chunker-section"><h2 id="h2-section-3">H2 Section 3</h2>
<p>Content section 3</p>
</div><div class="chunker-section"><h2 id="h2-section-4">H2 Section 4</h2>
<p>Content section 4</p>
</div><div class="chunker-section"><h2 id="h2-section-5">H2 Section 5</h2>
<p>Content section 4</p></div>
HTML;

    $this->assertEquals($expected, (string) $output);
  }

}
