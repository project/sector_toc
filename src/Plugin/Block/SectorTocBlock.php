<?php

namespace Drupal\sector_toc\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\sector_toc\Util\SectorTocHelper;
use Drupal\toc_api\Entity\TocType;
use Drupal\toc_api\Plugin\Block\TocBlockBase;

/**
 * Provides a table of contents block.
 *
 * @Block(
 *   id = "sector_toc_block",
 *   admin_label = @Translation("Sector Table of Contents block"),
 *   category = @Translation("Sector"),
 * )
 */
class SectorTocBlock extends TocBlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Load ToC definition, see also toc_api.toc_type.sector_toc.yml.
    $toc_type = TocType::load('sector_toc');
    $options = ($toc_type) ? $toc_type->getOptions() : [];

    // Create a TOC instance using the TOC manager.
    /** @var \Drupal\toc_api\TocManagerInterface $toc_manager */
    $toc_manager = \Drupal::service('toc_api.manager');
    /** @var \Drupal\toc_api\TocInterface $toc */
    $node = $this->getCurrentNode();
    if (is_object($node)) {
      $body = $node->body->value;
      if (!empty($body)) {

        $id = SectorTocHelper::generateTocId($toc_type->id(), $node, 'body', 0);
        $toc = $toc_manager->getToc($id) ?: $toc_manager->create($id, $body, $options);

        $toc_builder = \Drupal::service('toc_api.builder');
        $output = $toc_builder->buildToc($toc);

        return [
          'toc' => $output,
          '#attached' => [
            'library' => 'sector_toc/sector_toc',
          ],
        ];
      }
    }
    return [];
  }

}
