<?php

namespace Drupal\sector_toc\Util;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;

/**
 * Helper class for Sector ToC.
 */
class SectorTocHelper {

  /**
   * Generate an ID for a Table of Contents.
   *
   * @param string $toc_type
   *   The ID for the toc type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity from where the ToC value comes from.
   * @param string $field_name
   *   The field name where the ToC value comes from.
   * @param int $delta
   *   The field delta where the ToC value comes from.
   *
   * @return string
   *   ID to use for the ToC.
   */
  public static function generateTocId(string $toc_type, EntityInterface $entity, string $field_name, int $delta): string {
    $parts = [
      $toc_type,
      $entity->getEntityTypeId(),
      $entity->id(),
      $entity instanceof RevisionableInterface ? $entity->getRevisionId() : 0,
      $field_name,
      $delta,
    ];
    return implode(':', $parts);
  }

}
