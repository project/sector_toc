

Creates block to show in sidebar beside long HTML documents. Table of Contents links are generated to h2, h3 headings in node body field.

Adds javascript to actively update URL hash to indicate 'active' heading (via Intersection Observer). Attempts to ignore scrolling to a new document location in response to a click on a ToC hyperlink, as distinct from user scrolling while reading.

## Install

Per normal

## Configuration

Cause the Table of Contents block (SectorTocBlock) to display in the theme sidebar, e.g. via context reaction to content type is Resource, or via Block layout when Content type is Resource.

Change Resource display Default view mode Body field formatter to be 'ToC Chunker'; this parses the body content to inject id attributes onto HTML headings, so the table of contents links have a valid href target.
